# Shredir

Shred script to delete an entire directory.

## Installation

You can make this script confortable:

```bash
chmod +x shredir && mv shredir /usr/bin/shredir
```

## Usage

```bash
$# shredir

WARNING!!! This script will destroy all your directory without any option to recover it.
PLEASE BE CAREFULL!!!

Directory available:
<dir>

Directory name: <dir>
Directory selected: <dir>

Are you 100% SURE? [Y/N]: Y
...
...
Directory <dir> is now totally destroyed!
```
